# MTerm
MTerm is a terminal I decided to work on based around a VTE terminal tutoria. This terminal requires the DEC Terminal Modern font to be pre-installed to your computer. This font can be gotten here https://www.dafont.com/dec-terminal-modern.font and installed to any computer that utilizes .ttf files.

# What do you need?
`vre-2.91` and `libgtk-dev` are required so `vte.h` and `gtk.h` can be used.

# Notes
I'm a beginner. 
